import React from 'react'
import TextInput from './TextInput'

class CheckboxInput extends React.Component {
  constructor(props) {
    super(props)
    this.inputRef = React.createRef()
    this.state = {
      checked: false,
      email: ''
    }
  }
  handleChange = (event) => {
    const { target: { checked } = {} } = event
    const { id, onChange = () => {} } = this.props
    this.setState({ checked })
    this.inputRef.current?.focus()
    onChange(event, id)
  }
  onChangeText = (event) => {
    const { target: { value } = {} } = event
    const { onChange = () => {} } = this.props
    this.setState({ email: value })
    onChange(event, 'email')
  }
  render() {
    const {
      id,
      error = false,
      label = '',
      type = 'checkbox'
    } = this.props
    return (
      <>
        <label id={id} className={error ? 'error' : ''}>
          <p>{label}</p>
          <input type={type} onChange={this.handleChange} {...this.props}/>
        </label>
        {/* TODO separate logic*/}
        {this.state.checked && (
          <TextInput
            label="Email Adress"
            placeholder="Enter your Email Adress"
            onChange={this.onChangeText}
            inputRef={this.inputRef}
          />
        )}
      </>
    )
  }
}

export default CheckboxInput
