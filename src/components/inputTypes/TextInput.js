import React from 'react'

function TextInput(props) {
  const {
    id,
    label = '',
    error = false,
    placeholder = '',
    type = 'text',
    onChange = () => {}
  } = props
  return (
    <label id={id} className={error ? 'error' : ''}>
      <p>{label}</p>
      <input type={type} placeholder={placeholder} onChange={onChange} {...props}/>
    </label>
  )
}

export default TextInput
