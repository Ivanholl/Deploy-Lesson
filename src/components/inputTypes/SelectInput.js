import React from 'react'


function SelectInput(props) {
  const {
    id,
    label = '',
    error = false,
    placeholder = '',
    options = [],
    onChange = () => {}
  } = props
  return (
    <label id={id} className={error ? 'error' : ''}>
      <p>{label}</p>
      <select onChange={onChange} {...props}>
        <option value="">{placeholder}</option>
        {options.map((option) => {
          return (
            <option key={option?.label} value={option?.label}>
              {option?.label}
            </option>
          )
        })}
      </select>
    </label>
  )
}

export default SelectInput
