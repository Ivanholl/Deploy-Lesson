import React from 'react'
import TextInput from '../inputTypes/TextInput'
import SelectInput from '../inputTypes/SelectInput'
import CheckboxInput from '../inputTypes/CheckboxInput'

import './DynamicFormBuilder.scss'

class DynamicFormBuilder extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      fields: [],
      values: {}
    }
  }

  componentDidMount() {
    const { formFields: { fields = [] } = {} } = this.props
    if (!fields) return null
    // eslint-disable-next-line prefer-const
    let values = {}
    fields.forEach((field) => {
      values[field.id] = ''
    })
    this.setState({
      fields,
      values
    })
  }
  setError = (id) => {
    const newFields = this.state.fields.map((field) => {
      if (field.id === id) {
        return {
          ...field,
          error: true
        }
      }
      return {
        ...field,
        error: false
      }
    })
    this.setState({
      fields: newFields
    })
  }
  validateForm = () => {
    const { values, fields } = this.state
    let hasErr = true
    const newFields = fields.map((field) => {
      if (field.required && !values[field.id]) {
        hasErr = false
        return { ...field, error: true }
      }
      return { ...field, error: false }
    })
    this.setState({
      fields: newFields
    })
    return hasErr
  }

  onSubmit = (event) => {
    event.preventDefault()
    const isValid = this.validateForm()
    if (!isValid) return
    console.log('A form was submitted: ' + JSON.stringify(this.state.values))
  }

  onChange = (event, id) => {
    const { target: { value, checked } = {} } = event
    const newValues = {
      ...this.state.values,
      [id]: value || checked
    }
    this.setState({
      values: newValues
    })
  }

  getInputType = (inputData) => {
    const { id } = inputData
    switch (inputData.type) {
      case 'text':
        // TODO more meta
        return (
          <TextInput
            key={id}
            {...inputData}
            onChange={(event) => this.onChange(event, id)}
          />
        )
      case 'select':
        return (
          <SelectInput
            key={id}
            {...inputData}
            onChange={(event) => this.onChange(event, id)}
          />
        )
      case 'checkbox':
        return (
          <CheckboxInput
            key={id}
            {...inputData}
            onChange={(event, id) => this.onChange(event, id)}
          />
        )
      default:
        break
    }
  }
  renderOptions = ({
    placeholder = '',
    options = []
  }) => {
    <>
      <option value="">{placeholder}</option>
      {options.map((option) => {
        return (
          <option key={option?.label} value={option?.label}>
            {option?.label}
          </option>
        )
      })}
    </>
  }

  getInput= (inputData) => {
    const { 
      id,
      error = false,
      // placeholder = '',
      options = [],
      name = `name-${id}`,
      label = '',
      type = 'text'
    } = inputData
    const Component = type === 'select' ? <select/> : <input/>

    return (
      <label id={id} className={error ? 'error' : ''} htmlFor={name}>
        <p>{label}</p>
        <Component
          key={id}
          {...inputData}
          onChange={(event, id) => this.onChange(event, id)}
        >
          {options && 
            this.renderOptions(inputData)
          }
        </Component>
      </label>
    )
  }
  render() {
    const { fields } = this.state
    if (!fields) return null

    return (
      <form onSubmit={this.onSubmit}>
        {fields.map((formField) => this.getInput(formField))}
        <button type="submit">Submit</button>
      </form>
    )
  }
}

export default DynamicFormBuilder
